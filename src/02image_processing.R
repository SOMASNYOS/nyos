# Batch rename files to include sighting number and photographer

#install.packages("exifr")
install.packages("imager")
#devtools::install_github("dahtah/imager")
library(imager)
#library(exifr)
dir <- "/Users/eheywood/Google\ Drive/NYOS_digikam/cruise/"
dir_sys <- "/Users/eheywood/Google\\ Drive/NYOS_digikam/cruise/"
subDir <- "NYOS1907/"

day_camera <- "20190726/D750/"

files <- list.files(path = paste0(dir, subDir, day_camera), full.names = FALSE, recursive = TRUE)
# Invoke system command from R using system()

keyword_info <- rep(NA, length(files))
photographers <- c("EIH", "AB", "JES", "KL")
sight_nums <- paste0(rep("S", 1000), seq(1:1000))
spacer <- "Spacer"




for (i in 1:length(files)) {
  
  #system2(command = "cd", args = paste0(dir_sys, subDir))
  
  
  tags <- system2(command = "mdls", args = c("-name", "kMDItemKeywords", 
                                             paste0(dir_sys, subDir, day_camera, files[i])), stdout = TRUE)
  tags <- trimws(tags, which = c("both"))
  tags <- gsub(",", "", tags)
  pi_index <- which(tags %in% photographers) 
  pg <- tags[pi_index]
  s_index <- which(tags %in% sight_nums)
  s <- tags[s_index]
  spacer_index <- which(tags == "Spacer")
  spacer <- tags[spacer_index]
  # File rename - rename file[i] by replacing the "PI" with tags[pi_index] 
  new.name <- paste0(dir, subDir, day_camera, files[i])
  
  if (length(pi_index) > 0){
    file.rename(from = new.name, to = gsub("PI", pg, new.name))
    new.name <- gsub("PI", pg, new.name)
  }
  
  if (length(s_index) > 0){
    file.rename(from = new.name, to = gsub("S#", s, new.name))
    new.name <- gsub("S#", s, new.name)
  }
 
  if (length(spacer_index) > 0) {
    file.rename(from = new.name, to = gsub("S#", spacer, new.name))
  }
}






