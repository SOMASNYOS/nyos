# Created by: Eleanor Heywood
# Date: 06-10-2019
# Purpose: Extract on / off effort coordinates for MMO Obs

# load necessary packages

# install.packages("swfscMisc")
library(swfscMisc)

# set cruise ID and path to wincruz backup
nyos_cruise_id <- "NYB1905"
pway <- paste("/Users/eheywood/Documents/NYBCruiseBackup/", nyos_cruise_id, "/wincruz_backup", sep = "")
write_pway <- paste("/Users/eheywood/Documents/NYBCruiseBackup/", nyos_cruise_id, "/", sep = "")
# generate list of .das files to read in 
cruz_raw_files <- list.files(path = pway, full.names = TRUE, pattern = NULL) 

# batch read in wincruz .das file
x <- lapply(cruz_raw_files, das.read)

# row bind to combine into single data frame
marmam_obs_dataframe <- do.call(rbind, x)

# order this dataframe by timestamp
marmam_obs_dataframe <- marmam_obs_dataframe[order(marmam_obs_dataframe$Date), ]

# extract only date, effort, and coordinates
effort <- subset(marmam_obs_dataframe, select = c("Date", "OnEffort", "Lat", "Long"))

# write this out to csv for further processing / map generation
write.csv(effort, file = paste(write_pway, nyos_cruise_id, "effort", sep = ""))

# write out wincruz log

