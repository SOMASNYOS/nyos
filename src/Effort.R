# effort() is a function that ingests trackline data stream and effort spreadsheet from the NYOS Mysticetus template data collected during all NYB 
# offshore cruises from NYOS1907 onwards.  Cruises prior to 1907 used wincruz.  
# effort(wd, effortpway, tracklinepway, cruiseID)

# where the user must input the working directory where csvs are stored (wd)
# effortpway - the file pa


effort <- function(wd, cruiseID) {
  
  # Load necessary libraries:
  library(tidyverse)
  library(leaflet)
  
  # Set the working directory and subsequent data directories
  
  
  datapway <- file.path(paste(wd, cruiseID, "ExportedObs/", sep = "/"))
  
  # Generate list of csv's to read in to R
  # lists all of the csv files in the GBBG
  trackline_file_paths <- list.files(path = datapway, full.names = TRUE, pattern = "Track") 
  
  #read each one in and store them all in a list "alltracks_list"
  alltracks_list <- lapply(trackline_file_paths, read_csv)
  
  # trackline dataframe
  trackline_df <- do.call(rbind, alltracks_list)
  
  # read in effort csvs

  effort_file_paths <- dir(datapway, recursive = TRUE, full.names = TRUE, pattern = "Effort.csv")
  
  effort <- effort_file_paths %>% purrr::map(read_csv) %>%
    bind_rows() 
  
  effort <- effort %>% select(`Time (UTC)`, Effort, Reason)
  
  
  # merge effort and tracklines
  #make id type variable
  effort$TYPE <- "effort"
  trackline_df$TYPE <- 'track'
  effort$X <- rownames(effort)
  trackline_df$X <- rownames(trackline_df)
  
  # select small set necessary
  track_small <- select(trackline_df,  X, `Time Created (UTC)`, TYPE)
  effort_small <- select(effort, X, `Time (UTC)`, TYPE, Effort, Reason)
  
  # match variables
  track_small$Effort <- NA
  track_small$Reason <- NA
  colnames(track_small) <- c("X", "Time (UTC)", "TYPE", "Effort", "Reason")
  
  # bind together
  full <- rbind(track_small, effort_small)
  
  # sort by time order
  full$`Time (UTC)` <- as.POSIXct(full$`Time (UTC)`)
  
  full_sorted <- full[order(full$`Time (UTC)`), ]
  full_sorted$Day <- lubridate::day(full_sorted$`Time (UTC)`)
  
  full_sortedL <- split(full_sorted, full_sorted$Day)
  classified_effort <- list()
  
  for (i in 1:length(full_sortedL)){
    day <- full_sortedL[[i]]
    effortstatus <- "off"
    reason <- "before effort started for day"
    Effort_for_trackline <- vector()
    Reason_for_trackline <- vector()
    
    for (j in 1:nrow(day)){
      if (day$TYPE[j] == "track") {
        Effort_for_trackline[j] <- effortstatus
        Reason_for_trackline[j] <- reason
      } 
      if (day$TYPE[j] == "effort") {
        effortstatus <- day$Effort[j]
        reason <- day$Reason[j]
        Effort_for_trackline[j] <- effortstatus
        Reason_for_trackline[j] <- reason
      }
      
    }
    day$trackstatus <- Effort_for_trackline
    day$Reason <- Reason_for_trackline
    classified_effort[[i]] <- day
  }
  
  effort_classified <- do.call(rbind, classified_effort)
  
  effort <- effort_classified[effort_classified$TYPE == "track", ]
  
  
  effort_full <- merge(effort, trackline_df, "X")
  
  effort_full$effortID <- NA
  effortID <- 0
  
  effort_full <- effort_full %>% arrange(`Time (UTC)`)
  
  effort_fullL <- split(effort_full, effort_full$Day)
  effort_tracks <- list() #initialize empty list for effort stuff
  
  for (j in 1:length(effort_fullL)) {
    day = effort_fullL[[j]]
    day = arrange(day, `Time (UTC)`)
    day$effortID[1] <- effortID
    
    for (i in 2:nrow(day)){
    
    if(day$trackstatus[i] == day$trackstatus[i-1]){
      day$effortID[i] <- effortID
    }
    if(day$trackstatus[i] != day$trackstatus[i-1]){
      effortID <- effortID + 1
      day$effortID[i] <- effortID
    }
    
    } # end of i for loop
    # advance effortID for the next day
    effortID <- effortID + 1
    effort_tracks[[j]] <- day
} # end of j for loop
  
e <- do.call(rbind, effort_tracks)

return(e)

}

effort_map <- function(e, coordref_epsg) {
  effort_on <- e[e$trackstatus == "on", ]
  # CTD off
  effort_off <- e[e$trackstatus == "off" & e$Reason %in% c("before effort started for day", "Begin / Resume Effort", "Other (explain in notes)", "End of Day"), ] 
  close_on_sighting <- e[e$Reason == "Close on Sighting - ID / Photos", ]
  ctd <- e[e$Reason == "CTD", ]
  # Convert to an sf object to plot with leaflet
  on_locs <- sf::st_as_sf(effort_on, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs(coordref_epsg) #epsg number for WGS84
  off_locs<-  sf::st_as_sf(effort_off, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs(coordref_epsg) #epsg number for WGS84
  close_locs <- sf::st_as_sf(close_on_sighting, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs((coordref_epsg))
  ctd_locs <- sf::st_as_sf(ctd, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs((coordref_epsg))
  
  # Convert sf point locations to sf lines object for plotting in leaflet
  sf_lines_on <- on_locs %>%
    sf::st_geometry() %>% 
    sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(on_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
    sf::st_cast("MULTILINESTRING") %>% 
    sf::st_sf(effortID = as.factor(unique(on_locs$effortID)))
  
  sf_lines_off <- off_locs %>%
    sf::st_geometry() %>% 
    sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(off_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
    sf::st_cast("MULTILINESTRING") %>% 
    sf::st_sf(effortID = as.factor(unique(off_locs$effortID)))
  
  sf_lines_close <- close_locs %>%
    sf::st_geometry() %>% 
    sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(close_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
    sf::st_cast("MULTILINESTRING") %>% 
    sf::st_sf(effortID = as.factor(unique(close_locs$effortID)))
  
  sf_lines_ctd <- ctd_locs %>%
    sf::st_geometry() %>% 
    sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(ctd$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
    sf::st_cast("MULTILINESTRING") %>% 
    sf::st_sf(effortID = as.factor(unique(ctd$effortID)))

#pal <- RColorBrewer::brewer.pal(8, name = 'Dark2')
pal <- gray.colors(8, start = 0.3, end = 0.9, gamma = 2.2, alpha = NULL, rev = FALSE)
pal <- pal[c(1, 3, 5, 7)]
  
 m <- leaflet() %>% 
    addProviderTiles("Esri.OceanBasemap") %>% 
    addPolylines(data = sf_lines_on, weight = 4, color = pal[1], opacity = 0.85) %>% 
    addPolylines(data = sf_lines_off, weight = 4, color = pal[2], opacity = 0.85) %>%
    addPolylines(data = sf_lines_close, weight = 4, color = pal[3], opacity = 0.85) %>%
    addPolylines(data = sf_lines_ctd, weight = 4, color = pal[4], opacity = 0.85) %>%
    addLegend(position = "bottomright", color = pal, labels = c("On Effort", "Off Effort", "Close on Sighting", "CTD"))
  
  return(m)
}


