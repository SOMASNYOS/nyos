# script to map a bunch of stuff from NYOS cruise
monitorDir <- "/Users/eheywood/Documents/NYOS.monitoring"

subDir <- "NYOS1907" # change this based on the cruise ID... 
processedDir <- file.path(monitorDir, "processed")
dataDir <- file.path(monitorDir, "cruise.data")
srcDir <- file.path(monitorDir, "src")

coordref_epsg <- 4326

if (file.exists(file.path(processedDir, subDir))) {
  setwd(file.path(processedDir, subDir))
} else {
  dir.create(file.path(processedDir, subDir))
  setwd(file.path(processedDir, subDir))
}
  
processed_write_pway <- file.path(processedDir, subDir)
data_read_pway <- file.path(dataDir, subDir)

# source the effort function
source(file.path(srcDir, "effort_acoustic_cruise_track.R"))
source(file.path(srcDir, "readobs.R"))

# call in the map and the effort dataframe 
e1907 <- effort_acoustic_cruise_track(wd = dataDir, cruiseID = subDir)
e <- e1907
# map effort
m <- effort_map(e = e1907, coordref_epsg = 4326)
m

# add observations to it... 
obs <- readobs(datapway = dataDir, cruiseID = subDir, pattern = "Sighting.csv")

effort_on <- e[e$trackstatus == "on", ]
# CTD off
effort_off <- e[e$trackstatus == "off" & e$Reason %in% c("before effort started for day", "Begin / Resume Effort", "Other (explain in notes)", "End of Day"), ] 
close_on_sighting <- e[e$Reason == "Close on Sighting - ID / Photos", ]
ctd <- e[e$Reason == "CTD", ]
# Convert to an sf object to plot with leaflet
on_locs <- sf::st_as_sf(effort_on, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs(coordref_epsg) #epsg number for WGS84
off_locs<-  sf::st_as_sf(effort_off, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs(coordref_epsg) #epsg number for WGS84
close_locs <- sf::st_as_sf(close_on_sighting, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs((coordref_epsg))
ctd_locs <- sf::st_as_sf(ctd, coords = c("Longitude", "Latitude")) %>% sf::st_set_crs((coordref_epsg))
  
# convert obs to sf point object to plot with leaflet
obs_locs <- sf::st_as_sf(obs, coords = c("Sgt Long", "Sgt Lat")) %>% sf::st_set_crs(coordref_epsg)
  
# Convert sf point locations to sf lines object for plotting in leaflet
sf_lines_on <- on_locs %>%
sf::st_geometry() %>% 
sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(on_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
sf::st_cast("MULTILINESTRING") %>% 
sf::st_sf(effortID = as.factor(unique(on_locs$effortID)))

sf_lines_off <- off_locs %>%
sf::st_geometry() %>% 
sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(off_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
sf::st_cast("MULTILINESTRING") %>% 
sf::st_sf(effortID = as.factor(unique(off_locs$effortID)))
  
sf_lines_close <- close_locs %>%
sf::st_geometry() %>% 
sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(close_locs$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
sf::st_cast("MULTILINESTRING") %>% 
sf::st_sf(effortID = as.factor(unique(close_locs$effortID)))
  
sf_lines_ctd <- ctd_locs %>%
sf::st_geometry() %>% 
sf::st_cast("MULTIPOINT", ids = as.integer(as.factor(ctd$effortID))) %>% # error in attributes(ret) = attributes(x) : 'names' attribute [4423] must be the same length as the vector [73]
sf::st_cast("MULTILINESTRING") %>% 
sf::st_sf(effortID = as.factor(unique(ctd$effortID)))
  
#pal <- RColorBrewer::brewer.pal(8, name = 'Dark2')
pal <- gray.colors(8, start = 0.3, end = 0.9, gamma = 2.2, alpha = NULL, rev = FALSE)
pal <- pal[c(1, 3, 5, 7)]
spal <- RColorBrewer::brewer.pal(8, name = 'Dark2')
spal2 <- colorFactor(c(spal), domain = unique(obs_locs$Species1))
  
m <- leaflet() %>% 
  addProviderTiles("Esri.OceanBasemap") %>% 
  addPolylines(data = sf_lines_on, weight = 4, color = pal[1], opacity = 0.85, group = "Group A") %>% 
  addPolylines(data = sf_lines_off, weight = 4, color = pal[2], opacity = 0.85, group = "Group A") %>%
  addPolylines(data = sf_lines_close, weight = 4, color = pal[3], opacity = 0.85, group = "Group A") %>%
  addPolylines(data = sf_lines_ctd, weight = 4, color = pal[4], opacity = 0.85, group = "Group A") %>%
  addCircleMarkers(data = obs_locs, 
                   radius = 3.5,
                   color= ~spal2(Species1),
                   opacity = 1,
                   group = "Group B") %>%
    
    addLegend(color = pal, labels = c("On Effort", "Off Effort", "Close on Sighting", "CTD"), group = "Group A", position = "bottomright") %>%
    addLegend(pal = spal2, values = obs_locs$Species1, group = "Group B", position = "bottomright") %>%
  addLayersControl(overlayGroups = c("Group A", "Group B"))
  
    
m


# load in previous cruise observations 


# load NYOS 1905 effort and sightings
e1905 <- read_csv("/Users/eheywood/Documents/NYOS.monitoring/processed/NYOS1905/NYOS1905marmam_obs_effort.csv")


# Merge efforts 
names(e1907)
names(e1905)

e1907_trim <- e1907 %>% dplyr::select(`Time (UTC)`, Reason, Day, trackstatus, Latitude, Longitude, effortID) %>%
  mutate(effort.status = trackstatus) %>%
  select(-c(trackstatus))

e1905$effort.status <- ifelse(e1905$OnEffort == TRUE, "on", "off")
e1905_trim <- e1905 %>% mutate(Longitude = Long, Latitude = Lat, effortID = line_id, Reason = NA, Day = lubridate::day(`Time (UTC)`)) %>%
  dplyr::select(`Time (UTC)`, Reason, Day, Latitude, Longitude, effortID, effort.status) 

names(e1905_trim)
names(e1907_trim)

