####################
#SOMAS-NYOS Basemap Code v.1.1
#Last Update: 05 December 2019 - Brandyn Lucca
#Code is intended to generate the basemap layer for any additional plotting, such as cruise effort, data plotting, etc.
#This code uses the `ggplot2` plotting framework. 
#This code is accompanied with the required .shp files for coastline plotting. Make sure these are also downloaded and can specify their filepaths.
#Navigate to Line 20 of this script to edit the filepath accordingly; or replace the line with `file.choose()` to use an interactive pop-up to 
#manually navigate to the file(s) each time this script is run.
#You will also need to edit the filepath for where to save the basemap (end of script); or you can comment this out. 
####################
#Package dependences
pkg <- c("rgdal","marmap","ggplot2","gpclib","ggsn","rgeos")
#Call in packages
lapply(pkg, require, character.only=T)
#If any failures... `install.packages(...)` is the go-to! 
####################
#Call in coastline data
#The actual filepath for this will depend on where you save the shapefile/coastline data. Edit accordingly.
#This part of script converts the coastline shapefile into a cropped dataframe.
coastline <- raster::shapefile("C:/Users/ales/Documents/NYOS1910/Shapefiles/GSHHS_f_L1.shp") #Specifying the filepath
#coastline <- file.choose() #Manually selecting the shapefile filepath
coastline@data$id <- rownames(coastline@data)
coast_sub <- raster::crop(coastline, raster::extent(-74.5, -71.0, 38.5, 41.5))
####################
#Call in bathymetric data
#This part of script converts the bathymetric contours and accompanying data into a cropped dataframe. 
nybathy <- getNOAA.bathy(-74.5, -71, 38.5, 41.5, resolution=1); bathydf <- as.xyz(nybathy) 
bathydf$col <- NA
bathydf$col <- ifelse(bathydf$V3 >= -100, ifelse(bathydf$V3 < -15, "b","a"),"c")
bathydf$col <- factor(bathydf$col, levels=c("a",""))
####################
#Separate land from ocean.
#This part of script helps to properly overlay the coastline data on the background bathymetric layer since `marmap` can only provide very
#coarse coastline resolution. 
bathy_sea <- bathydf; bathy_sea$V3[bathy_sea$V3 > 1] <- NA
coast_fort <- fortify(coast_sub)
####################
#Generate basemap
#This part of script generates the actual basemap that can be used for future plotting functions.
#This part of the script is broken up by comments explaining the function of each part. Edit accordingly. 
basemap <- ggplot() + 
  #This creates bathymetric raster layer; smooth interpolation
  geom_raster(data=bathy_sea, aes(x=V1, y=V2, fill=V3)) +
  #This creates bathymetric contour lines at: 50, 100, 500, 1000, 2000, and 3000 m depth thresholds
  geom_contour(data=bathy_sea, aes(x=V1, y=V2, z=V3), 
               breaks=c(0,-50,-100,-500,-1000,-2000,-3000), colour="black") +
  #This creates coastline polygons
  geom_polygon(data=coast_fort, aes(x=long, y=lat, group=group), fill="gray60", color="black") +
  #This normalizes the x- and y-axes so there aren't any funky empty white space between the axis edges and the actual plot
  coord_equal(expand=c(0,0), ylim=c(38.5, 41.5), xlim=c(-74.5, -71)) +
  #Set latitudinal axis breaks to look prettier
  scale_y_continuous(breaks=c(39,40,41)) +
  #Set bathymetric gradient breaks
  scale_fill_gradient(limits=c(-3500,0), na.value="#9ecae1", low="#084594", high="#9ecae1",
                      breaks=c(-500,-1500,-2500,-3500), labels=c(500,1500,2500,3500)) +
  #Label axes
  labs(x=expression(paste("Longitude (",degree,"W)")),
       y=expression(paste("Latitude (",degree,"N)")),
       fill="Depth (m)") +
  #Setting some plotting parameters, like text size and background color
  theme_bw() +
  theme(text=element_text(size=18), axis.text=element_text(size=16, color="black"),
        strip.background=element_blank(), strip.text=element_blank()) +
  #Generate scalebar
  ggsn::scalebar(x.min=-74, x.max=-73.2, y.min=41.4, y.max=41.5,
                 dist=50, st.size=5, model="WGS84", dist_unit="km", transform=T,
                 height=0.4, st.dist=0.8) +
  #Generate North arrow
  ggsn::north(x.min=-74.5, x.max=-73.9, y.min=41, y.max=41.3, scale=1, symbol=3) +
  #Label bathymetric contours
  geom_text(aes(x=x, y=y, label=text),
            data=data.frame(x=c(rep(-71.23,3),rep(-71.28,2)), 
                            y=c(40.925, 40.025, 39.875, 39.75,39.55), 
                            text=c("50 m","100 m","500 m","1000 m","2000 m")),
            color=c(rep("black",3),rep("white",2)),size=5.5)
####################
#Print basemap.
basemap
#Save basemap
ggsave("C:/Users/ales/Documents/NYOS1910/NYOS_basemap.pdf", basemap, width=20, height=20, units="cm")
####################